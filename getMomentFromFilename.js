'use strict'

const Moment = require('moment')

const rxFritzWav = /\d{2}\.\d{2}\.\d{2}_\d{2}\.\d{2}/
const rxPhotoVideo = /\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}/
const rxPhoto = /\d{4}-\d{2}-\d{2}\s\d{2}\.\d{2}\.\d{2}/
const rxWinPhone = /\d{8}_\d{2}_\d{2}_\d{2}/

function getMomentFromFilename (fn) {
  let m
  if ((m = rxFritzWav.exec(fn))) {
    const mm = Moment(m[0], 'DD.MM.YY_HH.mm')
    if (mm.isValid()) { return mm }
  }
  if ((m = rxPhotoVideo.exec(fn))) {
    const mm = Moment(m[0], 'YYYY-MM-DD_HH-mm-ss')
    if (mm.isValid()) { return mm }
  }
  if ((m = rxPhoto.exec(fn))) {
    const mm = Moment(m[0], 'YYYY-MM-DD HH.mm.ss')
    if (mm.isValid()) { return mm }
  }
  if ((m = rxWinPhone.exec(fn))) {
    const mm = Moment(m[0], 'YYYYMMDD_HH_mm_ss')
    if (mm.isValid()) { return mm }
  }
  throw new Error(`Could not get moment from filename ${fn}`)
}

module.exports = getMomentFromFilename

