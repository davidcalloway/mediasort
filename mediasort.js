'use strict'

const Promise = require('bluebird')
const path = require('path')
const fs = Promise.promisifyAll(require('fs'))
const mkdirp = Promise.promisify(require('mkdirp'))
const exif = Promise.promisify(require('./exif'))
const getMomentFromFilename = require('./getMomentFromFilename')
const streamEqual = Promise.promisify(require('stream-equal'))
const Moment = require('moment')
const debug = require('debug')('mediasort')

const cliArgs = require('command-line-args')

const cliOptions = [{
  name: 'source',
  alias: 's',
  type: String,
  description: 'Source Directory (required)'
}, {
  name: 'destination',
  alias: 'd',
  type: String,
  description: 'Destination Directory (required)'
}, {
  name: 'action',
  alias: 'a',
  type: String,
  description: 'move, copy, or link (required)'
}, {
  name: 'keep',
  alias: 'k',
  type: Boolean,
  defaultValue: false,
  description: 'keep duplicate and empty files (instead of deleting)'
}, {
  name: 'verbose',
  alias: 'v',
  type: Boolean,
  defaultValue: false,
  description: 'verbose debug output'
}, {
  name: 'format',
  alias: 'f',
  defaultValue: 'YYYY/MM/DD',
  description: 'destination folder format (default YYYY/MM/DD)'
}, {
  name: 'help',
  alias: 'h',
  type: Boolean,
  defaultValue: false,
  description: 'display help'
}]

const opts = cliArgs(cliOptions)
const actions = ['move', 'copy', 'link']
if (opts.verbose) { debug.enabled = true }

if (opts.help || (!opts.source || !opts.destination || !opts.action)) {
  const cliUsage = require('command-line-usage')
  console.error(cliUsage([{
    header: 'mediasort',
    content: 'Moves/copies media from one directory to another'
  }, {
    header: 'Options',
    optionList: cliOptions
  }]))
  process.exit(1)
} else if (actions.indexOf(opts.action) === -1) {
  console.error('action must be set to one of: ' + actions.join(','))
  process.exit(1)
}

Promise.try(function ensureGoodDirs () {
  return Promise.all([
    fs.statAsync(opts.source),
    fs.statAsync(opts.destination)
  ])
}).spread(function checkDirs (dropStat, mediaStat) {
  if (!dropStat.isDirectory()) {
    throw new Error(`source "${opts.source}" not a directory`)
  } else if (!mediaStat.isDirectory()) {
    throw new Error(`destination "${opts.destination}" not a directory`)
  }
  return doSync()
}).catch(function quit (err) {
  debug(err)
  process.exit(1)
})

function doSync () {
  debug('Synching source=%s, destination=%s', opts.source, opts.destination)
  return syncDirectory(opts.source, true).then(function (total) {
    debug('Handled %d total items', total)
  }).catch(debug)
}

function syncDirectory (dir, keepDirAfterSync) {
  debug('Synching directory %s', dir)
  let total = 0
  return fs.readdirAsync(dir).then(function statItems (fileList) {
    fileList = fileList.map((f) => path.resolve(dir, f))
    return Promise.map(fileList, handleItem, {concurrency: 3})
  }).then(function addResults (results) {
    for (let subcount of results) { total += subcount }
    if (!keepDirAfterSync) {
      return deleteIfEmpty(dir)
    }
  }).then(() => total)
}

function deleteIfEmpty (dir) {
  return fs.readdirAsync(dir).then(function checkSize (fileList) {
    if (fileList.length !== 0) {
      debug('Not removing dir "%s" with %d files', dir, fileList.length)
      return false
    } else {
      debug('Removing empty dir "%s"', dir)
      return fs.rmdirAsync(dir).then(() => true)
    }
  })
}

function handleItem (fullPath) {
  return fs.statAsync(fullPath).then(function checkStat (stat) {
    if (stat.isDirectory()) { return syncDirectory(fullPath) }
    if (stat.isFile()) {
      return handleFile(fullPath, stat).then(() => 1)
    }
    debug('Cannot handle %s: %o', fullPath, stat)
    return 0
  })
}

function handleFile (filePath, stat) {
  if (stat.size === 0) {
    if (!opts.keep) {
      debug('Removing empty file "%s"', filePath)
      return fs.unlinkAsync(filePath)
    } else {
      return Promise.resolve(null)
    }
  }
  return getFileMoment(filePath, stat).then((moment) => {
    const subfolder = moment.format(opts.format)
    const targetDir = path.resolve(opts.destination, subfolder)
    return mkdirp(targetDir).then(() => targetDir)
  }).then((targetDir) => {
    const targetName = path.basename(filePath)
    return noCollideNoDupe(filePath, stat, targetDir, targetName, 0)
  }).then((info) => {
    return Actions[opts.action](filePath, info.file, info.duplicate)
  })
}

const Actions = {
  move (filePath, targetFile, isDupe) {
    if (isDupe) {
      if (!opts.keep) {
        debug('Existing duplicate found for "%s", deleting', filePath)
        return fs.unlinkAsync(filePath)
      } else {
        debug('Existing duplicate for "%s", passing', targetFile)
      }
    } else {
      debug('Moving: "%s" -> "%s"', filePath, targetFile)
      return fs.renameAsync(filePath, targetFile)
    }
  },
  copy (filePath, targetFile, isDupe) {
    if (!isDupe) {
      if (typeof fs.copyFileAsync === 'function') {
        debug('Copying: "%s" -> "%s"', filePath, targetFile)
        return fs.copyFileAsync(filePath, targetFile)
      } else {
        debug('fs.copyFile missing: "%s" -> "%s"', filePath, targetFile)
      }
    }
  },
  link (filePath, targetFile, isDupe) {
    if (!isDupe) {
      debug('Linking: "%s" -> "%s"', filePath, targetFile)
      return fs.linkAsync(filePath, targetFile)
    }
  }
}

function getFileMoment (filePath, stat) {
  return exif(filePath).then(function useExifMoment (d) {
    const dto = d && d.tags && d.tags.DateTimeOriginal
    if (!dto) {
      throw new Error('No exif.DateTimeOriginal')
    }
    let mm
    if (typeof dto === 'number') {
      mm = Moment.utc(dto * 1000)
    } else if (typeof dto === 'string') {
      // some manufacturers do it differently...
      mm = Moment(dto, 'YYYY/MM/DD HH:mm:ss')
    } else {
      throw new Error(`exif.DateTimeOriginal unknown type ${dto}`)
    }
    if (!mm.isValid()) {
      throw new Error(`exif.DateTimeOriginal bad Format ${dto}`)
    }
    return mm
  }).catch(function exifFailed (err) {
    const mm = getMomentFromFilename(path.basename(filePath))
    const ymdhms = mm.format('YYYY-MM-DD HH:mm:ss')
    debug('Got %s from filename after error: %s', ymdhms, err.message)
    return mm
  }).catch(function modTimeFallback (err) {
    debug('Using file mod time for file %s: %s', filePath, err.message)
    return Moment(stat.mtime)
  })
}

function noCollideNoDupe (origFile, origStat, targDir, targName, attempt) {
  const targFile = path.resolve(targDir, getAttemptFilename(targName, attempt))
  return fs.statAsync(targFile).then(function handleCollision (targStat) {
    // uh-oh: file collision: see if it's the same
    return isDuplicate(origFile, origStat, targFile, targStat).then((isDupe) => {
      if (isDupe) { return { duplicate: true, file: targFile } }
      return noCollideNoDupe(origFile, origStat, targDir, targName, attempt + 1)
    })
  }).catch(function handleTargetOK (err) {
    if (err.code !== 'ENOENT') { throw err }
    return { duplicate: false, file: targFile }
  })
}

function getAttemptFilename (targName, attempt) {
  if (!attempt) { return targName }
  const rxExt = /^(.*)(\.[A-Za-z][A-Za-z0-9]+)$/
  const m = rxExt.exec(targName)
  if (!m) { return targName + ` (${attempt})` }
  return m[1] + ` (${attempt})` + m[2]
}

function isDuplicate (origFile, origStat, targFile, targStat) {
  if (origStat.size !== targStat.size) { return Promise.resolve(false) }
  if (origStat.dev === targStat.dev && origStat.ino === targStat.ino) {
    return Promise.resolve(true)
  }
  debug('file "%s" size %d, checking bytes', targFile, targStat.size)
  const origRS = fs.createReadStream(origFile)
  const targRS = fs.createReadStream(targFile)
  return streamEqual(origRS, targRS)
}
