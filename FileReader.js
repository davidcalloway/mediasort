'use strict'

const fs = require('fs')

class FileReader {
  constructor (fd) {
    this.fd = fd
    this.offset = 0
  }
  seek (incr) { this.offset += incr }
  read (buff, cb) {
    if (!Buffer.isBuffer(buff)) {
      throw new Error('FileOffset.read() requires Buffer to fill!')
    }
    const size = buff.length
    fs.read(this.fd, buff, 0, size, this.offset, (err, bytesRead) => {
      if (err) { return cb(err) }
      this.seek(size)
      cb(null, buff)
    })
  }
  close (cb) {
    fs.close(this.fd, cb)
  }
}

module.exports = FileReader
