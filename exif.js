'use strict'

const FileReader = require('./FileReader')
const fs = require('fs')
const debug = require('debug')('exif')
const exifParser = require('exif-parser')

module.exports = fastExif

function fastExif (filename, cb) {
  const buffers = []
  let offset = 0
  function pushBuffer (buf, name) {
    let hex
    if (buf.length <= 10) {
      hex = buf.toString('hex')
    } else {
      hex = buf.slice(0, 3).toString('hex') + '...' + buf.slice(-3).toString('hex')
    }
    debug('Adding %d bytes (%s): "%s"', buf.length, hex, name || '???')
    debug('  Offset %s:%s', offset.toString(16), (offset + buf.length).toString(16))
    offset += buf.length
    buffers.push(buf)
  }
  let fr
  function fail (reason) {
    if (!(reason instanceof Error)) { reason = new Error(reason) }
    close(function finalize (err) { cb(err || reason) })
  }
  function succeed (exifData) {
    close(function finalize (err) { cb(err, exifData) })
  }
  function close (next) { fr ? fr.close(next) : next() }
  debug('opening %s', filename)
  fs.open(filename, 'r', function gotDescriptor (err, descriptor) {
    if (err) { return fail(err) }
    fr = new FileReader(descriptor)
    fr.read(Buffer.allocUnsafe(2), readSOI)
  })
  function readSOI (err, buffer) { // Start of Image
    if (err) { return fail(err) }
    if (buffer[0] !== 0xff || buffer[1] !== 0xd8) {
      return fail(new Error(`SOI 0xFFD8 not found in file`))
    }
    pushBuffer(buffer, 'SOI')
    fr.read(Buffer.allocUnsafe(2), readSectionType)
  }
  function readSectionType (err, buffer) {
    if (err) { return fail(err) }
    if (buffer[0] !== 0xff) {
      return fail(new Error(`Non-Marker byte, expected 0xFF != ${buffer[0]}`))
    }
    if (buffer[1] === 0xd9) {
      fail(new Error('End of Image w/o EXIF'))
    } else if (buffer[1] !== 0xda) {
      pushBuffer(buffer, 'SectionType')
      fr.read(Buffer.allocUnsafe(2), readSectionLength)
    } else {
      try {
        const allBuffers = Buffer.concat(buffers)
        debug('Passing %d bytes (in %d buffers) to exifParser', allBuffers.length, buffers.length)
        const parser = exifParser.create(allBuffers)
        const result = parser.parse()
        succeed(result)
      } catch (e) {
        fail(e)
      }
    }
  }
  function readSectionLength (err, buffer) {
    if (err) { return fail(err) }
    const len = buffer.readUInt16BE(0) - 2
    if (len < 0) { return fail(new Error(`Invalid SectionLength=${len}`)) }
    pushBuffer(buffer, 'SectionLength')
    fr.read(Buffer.allocUnsafe(len), readSectionData)
  }
  function readSectionData (err, buffer) {
    if (err) { return fail(err) }
    pushBuffer(buffer, 'SectionData')
    fr.read(Buffer.allocUnsafe(2), readSectionType)
  }
}
